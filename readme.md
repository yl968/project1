
# Deploying Zola Site to AWS S3
This README outlines the steps to deploy a Zola static site to Amazon S3
The website is running at: http://indproj1.s3-website-us-east-1.amazonaws.com, https://project1-yl968-663d41ef2c140d3e659721623886adc578dc40890f37b1bf.pages.oit.duke.edu/

## Prerequisites

- AWS account
- Zola site in a Git repository

## Step 1: Prepare Zola Site

Ensure Zola site builds locally without errors:

```bash
zola build
```

This command generates site's static files in the `public` directory.

## Step 2: Create and Configure S3 Bucket

1. **Log in to AWS Management Console** and navigate to the S3 service.
2. **Create a new bucket** named `indproj1`, ensuring the "Block all public access" settings are unchecked.
3. **Enable Static Website Hosting**:
   - Go to the **Properties** tab of  bucket.
   - Select **Static website hosting**.
   - Choose **Host a static website**.
   - Set both the Index and Error documents to `index.html`.

## Step 3: Set Up Bucket Permissions

1. **Edit the Bucket Policy**:
   - Navigate to the **Permissions** tab and select **Bucket Policy**.
   - Add a public read access policy:

     ```json
     {
       "Version": "2012-10-17",
       "Statement": [{
         "Sid": "PublicReadGetObject",
         "Effect": "Allow",
         "Principal": "*",
         "Action": "s3:GetObject",
         "Resource": "arn:aws:s3:::indproj1/*"
       }]
     }
     ```

## Step 4: Upload Site to S3

Upload the contents of the `public` directory to S3 bucket. This can be done via the AWS Management Console, AWS CLI, or a CI/CD pipeline.

Using AWS CLI:

```bash
aws s3 sync public s3://indproj1/ --delete
```

Using CI/CD pipeline:

```bash
deploy_to_s3:
  stage: deploy
  image: python:latest
  script:
    - pip install awscli
    - aws s3 sync public s3://indproj1 --delete
  only:
    - main

```

## Step 5: Accessing  Website

 Zola site is now accessible via the S3 bucket's website endpoint, found in the **Static website hosting** section of  bucket's **Properties** tab. In my case, the url is: http://indproj1.s3-website-us-east-1.amazonaws.com
![image](7602a49639b715087af9d994bcd201f.png)


## Conclusion
 Zola site should now be live on AWS S3
 Demo: [video](yuanzhe _ Project1 · GitLab - Google Chrome 2024-03-09 19-27-25.mp4)
